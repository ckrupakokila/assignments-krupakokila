//append file

var fs=require('fs')
fs.appendFile('file.text','Hello content',function(err){
    if(err) throw err;
    console.log('saved')
})

//delete file

var fs=require('fs')
fs.unlink('file2.text',function(err){
    if(err) throw err;
    console.log('deleted')
})

//write file can perform the previous content replace with new content

var fs=require('fs')
fs.writeFile('file.text','welcome Node js',function(err){
    if(err) throw err;
    console.log('replaced')
})

//Rename filename

 var fs=require('fs')
 fs.rename('file.text','file1.text',function(err){
     if(err) throw err;
     console.log('file renamed')
 })